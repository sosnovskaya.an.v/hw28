﻿#include <iostream>
#include <math.h>

class Vector 
{
public:
    Vector(): x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double GetX() 
    {
        return x;
    }
    void SetX(double newX) 
    {
        x = newX;
    }
    double GetY() 
    {
        return y;
    }
    void SetY(double newY) 
    {
        y = newY;
    }
    double GetZ() 
    {
        return z;
    }
    void SetZ(double newZ) 
    {
        z = newZ;
    }
    double Length() 
    {
        return sqrt(x*x+y*y+z*z);
    }
    void Show() {
        std::cout << "Vector(" << x << "," << y << "," << z << ")\n";
    }
private:
    double x;
    double y;
    double z;


};

int main()
{
    Vector vector1(3,4,5);
    vector1.Show();
    std::cout << "Vector length:\n"<<vector1.Length();
}
